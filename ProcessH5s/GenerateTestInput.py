import h5py
import numpy as np

# Number of events and jets per event
num_events = 10
jets_per_event = 5

# Generate synthetic data
event_numbers = np.repeat(np.arange(num_events), jets_per_event)
etas = np.random.uniform(-5, 5, num_events * jets_per_event)
phis = np.random.uniform(-np.pi, np.pi, num_events * jets_per_event)
GN2v01_pb = np.random.uniform(0, 1, num_events * jets_per_event)
GN2v01_pc = np.random.uniform(0, 1, num_events * jets_per_event)
GN2v01_pl = np.random.uniform(0, 1, num_events * jets_per_event)
GN2v01_ptau = np.random.uniform(0, 1, num_events * jets_per_event)

pts = np.random.uniform(20, 200, num_events * jets_per_event)

# File path to save the HDF5 file
file_path = 'test_jets.h5'

# Create HDF5 file and write data
with h5py.File(file_path, 'w') as h5file:
    jets_group = h5file.create_group('jets')
    jets_group.create_dataset('eventNumber', data=event_numbers)
    jets_group.create_dataset('eta', data=etas)
    jets_group.create_dataset('phi', data=phis)
    jets_group.create_dataset('GN2v01_pb', data=GN2v01_pb)
    jets_group.create_dataset('GN2v01_pc', data=GN2v01_pc)
    jets_group.create_dataset('GN2v01_pl', data=GN2v01_pl)
    jets_group.create_dataset('GN2v01_ptau', data=GN2v01_ptau)
    jets_group.create_dataset('pt', data=pts)

print(f"HDF5 file '{file_path}' created with synthetic data.")