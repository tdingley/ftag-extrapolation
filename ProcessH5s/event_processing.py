import os
import h5py
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import argparse
import logging
import ROOT as root
import atlas_mpl_style as ampl
from typing import Tuple, List
from scipy.interpolate import UnivariateSpline
import time
# Setup matplotlib with a more universally available font
plt.rcParams['font.family'] = 'DejaVu Sans'
plt.rcParams['font.sans-serif'] = ['DejaVu Sans', 'Arial', 'sans-serif']
ampl.use_atlas_style()

# Setup logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Dictionary to map variable names to their plot labels and styles
variable_labels = {
    'pt': r'$p_T$ [GeV]',
    'eta': r'$\eta$',
    'phi': r'$\phi$',
    'GN2v01_pb': 'GN2v01_pb',
    'GN2v01_pc': 'GN2v01_pc',
    'GN2v01_pu': 'GN2v01_pu',
    'GN2v01_ptau': 'GN2v01_ptau',
    'HadronConeExclTruthLabelID': 'HadronConeExclTruthLabelID',
    'discriminant': 'Discriminant',
    'dphi_min': r'$\Delta\phi_{min}$'
}

def calculate_dphi(phi1, phi2):
    """
    Calculate the delta phi between two angles.
    """
    dphi = np.abs(phi1 - phi2)
    return np.minimum(dphi, 2 * np.pi - dphi)

def get_cutvalue(tagger: str = "GN2v01", 
                 cdi_file: str = '/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2023-02_MC20_CDI_GN2v01-noSF.root', 
                 jet_collection: str = "AntiKt4EMPFlowJets", 
                 wp: int = 65) -> Tuple[List[float], float, float]:
    """Retrieve cut values and fractions from the CDI file."""
    start_time = time.time()

    cdi_file = root.TFile.Open(cdi_file)
    if not cdi_file or cdi_file.IsZombie():
        raise RuntimeError("Failed to open CDI file.")
    
    cut_value_arr = []
    TVectorT_float = root.TVectorT('float')

    for wp in [65, 70, 77, 85, 90]:
        dir_path = f"/{tagger}/{jet_collection}/FixedCutBEff_{wp}/"
        dir = cdi_file.GetDirectory(dir_path)
        if not dir:
            raise RuntimeError(f"Failed to navigate to directory: {dir_path}")
        
        logging.info(f"Getting cut value from: {dir_path}")
        
        cut_value = dir.Get("cutvalue")
        if isinstance(cut_value, (root.TVectorD, TVectorT_float)):
            cut_value_arr.append(cut_value[0])
        else:
            raise ValueError(f"Unknown type: {type(cut_value)}")
       
    fc = dir.Get("fraction")
    ftau = dir.Get("fraction_tau")

    if isinstance(fc, (root.TVectorD, TVectorT_float)):
        fc_val = fc[0] 
    else:
        raise ValueError(f"Unknown type: {type(fc)}")
    if isinstance(ftau, (root.TVectorD, TVectorT_float)):
        ftau_val = ftau[0]
    else:
        raise ValueError(f"Unknown type: {type(ftau)}")

    elapsed_time = time.time() - start_time
    logging.info(f"Time taken to scrape data: {elapsed_time} seconds")
    return cut_value_arr, fc_val, ftau_val
    
def read_h5_file(file_path, systematic):
    """
    Read data from an HDF5 file and return as a DataFrame.
    
    Parameters:
    - file_path (str): Path to the HDF5 file.
    - systematic (str): The systematic variation to look for.
    
    Returns:
    - pd.DataFrame: DataFrame containing the data from the HDF5 file.
    """
    jets_path = f'{systematic}_STANDARD_poor/jets' if systematic else 'STANDARD_poor/jets'
    
    with h5py.File(file_path, 'r') as h5file:
        if jets_path not in h5file:
            logging.error(f"Error: {jets_path} not found in {file_path}")
            return None

        jets = h5file[jets_path]

        data = {
            'eventNumber': jets['eventNumber'][:],
            'eta': jets['eta'][:],
            'phi': jets['phi'][:],
            'GN2v01_pb': jets['GN2v01_pb'][:],
            'GN2v01_pc': jets['GN2v01_pc'][:],
            'GN2v01_pu': jets['GN2v01_pu'][:],
            'GN2v01_ptau': jets['GN2v01_ptau'][:],
            'pt': jets['pt'][:] / 1e3,  # Convert to GeV
            'HadronConeExclTruthLabelID': jets['HadronConeExclTruthLabelID'][:]
        }
        df = pd.DataFrame(data)
        return df

def apply_corrections(df):
    """
    Apply corrections to the DataFrame.
    
    Parameters:
    - df (pd.DataFrame): DataFrame to apply corrections to.
    
    Returns:
    - pd.DataFrame: Corrected DataFrame.
    """
    df['HadronConeExclTruthLabelID'] = df['HadronConeExclTruthLabelID'].apply(lambda x: x if x in [0, 4, 5] else 0)
    return df

def collect_events(df):
    """
    Collect events using eventNumber.
    
    Parameters:
    - df (pd.DataFrame): DataFrame containing the data.
    
    Returns:
    - dict: Dictionary where keys are eventNumbers and values are DataFrames for each event.
    """
    events = {event_number: event_df for event_number, event_df in df.groupby('eventNumber')}
    return events

def calculate_dphi_min(events):
    """
    Calculate the minimum delta phi between the leading b-jet and all other jets in each event.
    
    Parameters:
    - events (dict): Dictionary of events.
    
    Returns:
    - pd.Series: Series containing dphi_min for each jet.
    """
    dphi_min_dict = {}

    for event_number, event_df in events.items():
        bjets = event_df[event_df['HadronConeExclTruthLabelID'] == 5]
        if bjets.empty:
            continue

        leading_bjet = bjets.loc[bjets['pt'].idxmax()]
        other_jets = event_df[event_df.index != leading_bjet.name]
        if other_jets.empty:
            dphi_min_dict.update({idx: np.nan for idx in bjets.index})
            continue

        dphi_values = calculate_dphi(leading_bjet['phi'], other_jets['phi'])
        dphi_min = dphi_values.min()
        dphi_min_dict.update({idx: dphi_min for idx in bjets.index})

    return pd.Series(dphi_min_dict)

def plot_variable(df, variable, output_dir):
    """
    Plot a histogram for a given variable.
    
    Parameters:
    - df (pd.DataFrame): DataFrame containing the data.
    - variable (str): Variable to plot.
    - output_dir (str): Directory to save the plot.
    """
    fig, ax = plt.subplots()
    ax.hist(df[variable], bins=50, alpha=0.75)
    ax.set_xlabel(variable_labels.get(variable, variable))
    ax.set_ylabel('Frequency')
    ax.set_title(f'Histogram of {variable_labels.get(variable, variable)}')
    ax.grid(True)
    ampl.draw_atlas_label(
        0.05, 0.95, ax=ax, status='int', simulation=True, 
        energy='13 TeV', lumi=140, 
        desc="AntiKt4EMPFlowJets", fontsize=15
    )
    
    output_path = os.path.join(output_dir, f'{variable}.pdf')
    plt.savefig(output_path)
    plt.close()

def plot_efficiency(df, variable, cut_values, output_dir):
    """
    Plot efficiency as a function of a given variable.
    
    Parameters:
    - df (pd.DataFrame): DataFrame containing the data.
    - variable (str): Variable to plot efficiency against.
    - cut_values (list): List of cut values for different working points.
    - output_dir (str): Directory to save the plot.
    """
    bin_edges = np.linspace(df[variable].min(), df[variable].max(), 50)
    bin_centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])
    
    fig, ax = plt.subplots()
    line_styles = ['-', '--', '-.', ':', (0, (3, 1, 1, 1))]  # Different line styles for each WP
    colors = plt.cm.viridis(np.linspace(0, 1, len(cut_values)))  # Use a colormap for distinct colors

    for cut_value, wp, line_style, color in zip(cut_values, [65, 70, 77, 85, 90], line_styles, colors):
        tagged = df[df['discriminant'] > cut_value]
        total, _ = np.histogram(df[variable], bins=bin_edges)
        passed, _ = np.histogram(tagged[variable], bins=bin_edges)
        efficiency = np.divide(passed, total, out=np.zeros_like(passed, dtype=float), where=total != 0)

        # Smooth the efficiency curve using a UnivariateSpline
        #spline = UnivariateSpline(bin_centers, efficiency, s=0.5)
        #smoothed_efficiency = spline(bin_centers)

        ax.plot(bin_centers, efficiency, label=f'{wp}% WP', linestyle=line_style, color=color)
    
    ax.set_xlabel(variable_labels.get(variable, variable))
    ax.set_ylabel('Efficiency')
    ax.set_title(f'Efficiency as a function of {variable_labels.get(variable, variable)}')
    ax.legend(loc='upper left', bbox_to_anchor=(1, 1), fontsize=12)
    ampl.draw_atlas_label(
        0.05, 0.95, ax=ax, status='int', simulation=True, 
        energy='13 TeV', lumi=140, 
        desc="AntiKt4EMPFlowJets", fontsize=15
    )
    ax.set_ylim(0, 1)

    # Adjust layout to make space for the legend
    fig.tight_layout(rect=[0, 0, 0.85, 1])
    # Or use fig.subplots_adjust(right=0.75)
    
    output_path = os.path.join(output_dir, f'efficiency_{variable}.pdf')
    plt.savefig(output_path)
    plt.close()

def plot_dphi_min_efficiency(df, cut_values, output_dir):
    """
    Plot efficiency as a function of dphi_min with a smoothed curve.
    
    Parameters:
    - df (pd.DataFrame): DataFrame containing the data.
    - cut_values (list): List of cut values for different working points.
    - output_dir (str): Directory to save the plot.
    """
    bin_edges = np.linspace(0, np.pi, 50)
    bin_centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])
    
    fig, ax = plt.subplots()
    line_styles = ['-', '--', '-.', ':', (0, (3, 1, 1, 1))]  # Different line styles for each WP
    colors = plt.cm.viridis(np.linspace(0, 1, len(cut_values)))  # Use a colormap for distinct colors

    for cut_value, wp, line_style, color in zip(cut_values, [65, 70, 77, 85, 90], line_styles, colors):
        tagged = df[df['discriminant'] > cut_value]
        total, _ = np.histogram(df['dphi_min'], bins=bin_edges)
        passed, _ = np.histogram(tagged['dphi_min'], bins=bin_edges)
        efficiency = np.divide(passed, total, out=np.zeros_like(passed, dtype=float), where=total != 0)

        # Smooth the efficiency curve using a UnivariateSpline
        spline = UnivariateSpline(bin_centers, efficiency, s=0.5)
        smoothed_efficiency = spline(bin_centers)

        ax.plot(bin_centers, smoothed_efficiency, label=f'{wp}% WP', linestyle=line_style, color=color)

    ax.set_xlabel(variable_labels['dphi_min'])
    ax.set_ylabel('Efficiency')
    ax.set_title('Efficiency as a function of $\Delta\phi_{min}$')
    ampl.draw_atlas_label(
        0.05, 0.95, ax=ax, status='int', simulation=True, 
        energy='13 TeV', lumi=140, 
        desc="AntiKt4EMPFlowJets", fontsize=15
    )
    
    ax.legend(loc='upper left', bbox_to_anchor=(1, 1), fontsize=12)
    ax.set_ylim(0, 1)

    # Adjust layout to make space for the legend
    fig.tight_layout(rect=[0, 0, 0.85, 1])
    # Or use fig.subplots_adjust(right=0.75)

    output_path = os.path.join(output_dir, 'efficiency_dphi_min.pdf')
    plt.savefig(output_path)
    plt.close()


def plot_dphi_min_histogram(df, cut_values, output_dir):
    """
    Plot dphi_min as a histogram with 10 bins for each working point.
    
    Parameters:
    - df (pd.DataFrame): DataFrame containing the data.
    - cut_values (list): List of cut values for different working points.
    - output_dir (str): Directory to save the plot.
    """
    fig, ax = plt.subplots()
    bin_edges = np.linspace(0, np.pi, 11)  # 10 bins from 0 to pi

    for cut_value, wp in zip(cut_values, [65, 70, 77, 85, 90]):
        tagged = df[df['discriminant'] > cut_value]
        ax.hist(tagged['dphi_min'], bins=bin_edges, alpha=0.5, label=f'{wp}% WP')

    ax.set_xlabel(variable_labels['dphi_min'])
    ax.set_ylabel('Frequency')
    ax.set_title('Histogram of $\Delta\phi_{\text{min}}$')
    ax.grid(True)
    ampl.draw_atlas_label(
        0.05, 0.95, ax=ax, status='Simulation Internal', simulation=True, 
        energy='13 TeV', lumi=140, 
        desc="AntiKt4EMPFlowJets", fontsize=15
    )
    ax.legend(loc='upper left', bbox_to_anchor=(1, 1), fontsize=12)

    output_path = os.path.join(output_dir, 'dphi_min_histogram.pdf')
    plt.savefig(output_path)
    plt.close()



def process_h5_files(input_directory, output_directory, tagger="GN2v01", 
                     cdi_file='/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2023-02_MC20_CDI_GN2v01-noSF.root', 
                     jet_collection="AntiKt4EMPFlowJets"):
    """
    Process all HDF5 files in the input directory.
    
    Parameters:
    - input_directory (str): Path to the directory containing HDF5 files.
    - output_directory (str): Path to the directory to save output plots.
    - tagger (str): Tagger to use for getting cut values.
    - cdi_file (str): Path to the CDI file for getting cut values.
    - jet_collection (str): Jet collection to use for getting cut values.
    
    Returns:
    - dict: Dictionary of events collected from all HDF5 files.
    """
    cut_values, fc, ftau = get_cutvalue(tagger, cdi_file, jet_collection)
    
    all_events = {}
    for subdir, _, files in os.walk(input_directory):
        for file in files:
            if file.endswith(".h5"):
                file_path = os.path.join(subdir, file)
                logging.info(f"Processing file: {file_path}")
                
                systematic = ""
                if "TRK_" in file:
                    parts = file.split(".")
                    for part in parts:
                        if part.startswith("TRK_"):
                            systematic = part
                            break
                
                # Read the HDF5 file
                df = read_h5_file(file_path, systematic)
                if df is None:
                    continue
                
                # Apply corrections
                df = apply_corrections(df)
                
                # Calculate the discriminant for each jet
                df['discriminant'] = np.log(df['GN2v01_pb'] / (df['GN2v01_pu'] * (1 - fc - ftau) + df['GN2v01_ptau'] * ftau + df['GN2v01_pc'] * fc))
                
                # Collect events
                events = collect_events(df)
                
                # Filter to include only b-jets (HadronConeExclTruthLabelID == 5)
                bjets = df[df['HadronConeExclTruthLabelID'] == 5]
                
                # Calculate dphi_min for b-jets
                bjets['dphi_min'] = calculate_dphi_min(events).reindex(bjets.index)

                # Merge events into all_events
                all_events.update(events)

                # Create output directories
                systematic_output_dir = os.path.join(output_directory, systematic if systematic else "nominal")
                os.makedirs(systematic_output_dir, exist_ok=True)
                all_output_dir = os.path.join(systematic_output_dir, "all")
                os.makedirs(all_output_dir, exist_ok=True)
                bjets_output_dir = os.path.join(systematic_output_dir, "bjets")
                os.makedirs(bjets_output_dir, exist_ok=True)
                
                # Plot each variable for all jets
                for variable in df.columns:
                    if variable != 'eventNumber' and variable != 'dphi_min':
                        variable_output_dir = os.path.join(all_output_dir, variable)
                        os.makedirs(variable_output_dir, exist_ok=True)
                        plot_variable(df, variable, variable_output_dir)
                        plot_efficiency(df, variable, cut_values, variable_output_dir)
                
                # Plot dphi_min for b-jets only
                variable_output_dir = os.path.join(bjets_output_dir, 'dphi_min')
                os.makedirs(variable_output_dir, exist_ok=True)
                plot_variable(bjets, 'dphi_min', variable_output_dir)
                plot_efficiency(bjets, 'dphi_min', cut_values, variable_output_dir)
                plot_dphi_min_histogram(bjets, cut_values, variable_output_dir)
                plot_dphi_min_efficiency(bjets, cut_values, variable_output_dir)

    return all_events

def main(input_directory, output_directory):
    """
    Main function to process HDF5 files, collect events, and generate plots.
    
    Parameters:
    - input_directory (str): Directory containing the input HDF5 files.
    - output_directory (str): Directory to save the output plots.
    """
    events = process_h5_files(input_directory, output_directory)
    logging.info(f"Collected {len(events)} unique events")

    # Here you can continue processing the events as needed
    # For now, we simply return the collected events
    return events

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process HDF5 files, collect events, and generate plots.")
    parser.add_argument("input_directory", help="Directory containing the input HDF5 files.")
    parser.add_argument("output_directory", help="Directory to save the output plots.")

    args = parser.parse_args()
    main(args.input_directory, args.output_directory)