#!/usr/bin/env python3

import os
import ROOT as root
import logging
import argparse

# Setup logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def combine_root_files(output_folder: str, systematic_type: str):
    """Combine ROOT files into a single file."""
    combined_root_file = root.TFile(os.path.join(output_folder, f"{systematic_type}_combined.root"), "RECREATE")

    for root_file_name in os.listdir(output_folder):
        if root_file_name.endswith("_output.root"):
            root_file_path = os.path.join(output_folder, root_file_name)
            with root.TFile(root_file_path, "READ") as input_root_file:
                folder_name = root_file_name.split("_output.root")[0]
                combined_root_file.mkdir(folder_name)
                combined_root_file.cd(folder_name)
                for key in input_root_file.GetListOfKeys():
                    obj = key.ReadObj()
                    obj.Write()
                combined_root_file.cd()

    combined_root_file.Close()
    logging.info(f"All ROOT files combined into '{os.path.join(output_folder, f'{systematic_type}_combined.root')}'.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Combine ROOT files after HTCondor jobs are completed.")
    parser.add_argument("output_folder", help="Output folder containing the individual ROOT files.")
    parser.add_argument("systematic_type", choices=["tracking", "jet", "partonshower"], help="Specify which type of systematic you're processing (tracking, jet or partonshower)")
    args = parser.parse_args()

    combine_root_files(args.output_folder, args.systematic_type)