import os
import argparse
import subprocess

def main(input_directory: str, tag: str, tag_map: dict, systematic_type: str, flavour: str):
    r_tag = tag_map.get(tag)
    if not r_tag:
        raise ValueError(f"Invalid tag specified: {tag}")

    # Define maps for each systematic type
    varied_maps = {
        "tracking": ["TRK_"],
        "jet": ["JET_"],
        "partonshower": ["525819"],
        "altgen": ["70"]
    }

    nominal_maps = {
        "tracking": ["nominal"],
        "jet": ["nominal"],
        "partonshower": ["521296"],
        "altgen": ["50", "51"]
    }
    
    condor_submit_content = """
    universe = vanilla
    executable = wrapper.sh
    arguments = {input_directory} {tag} {systematic_type} $(systematic) {flavour}
    output = logs/$(systematic).out
    error = logs/$(systematic).err
    log = logs/$(systematic).log
    queue systematic in {systematics}
    """

    os.makedirs("logs", exist_ok=True)
    os.makedirs("submit", exist_ok=True)

    systematics = set()
    for subdir, _, _ in os.walk(input_directory):
        if r_tag in subdir:
            parts = subdir.split(".")
            # Check for nominal maps
            for nominal_map in nominal_maps[systematic_type]:
                if nominal_map in subdir:
                    nominal_systematic = next((part for part in parts if part.startswith(nominal_map)), None)
                    if nominal_systematic:
                        systematics.add(nominal_systematic)

            # Check for varied maps
            for varied_map in varied_maps[systematic_type]:
                varied_systematic = next((part for part in parts if part.startswith(varied_map)), None)
                if varied_systematic:
                    systematics.add(varied_systematic)
                    break  # Assumption: only one varied systematic per subdir

    if not systematics:
        print(f"No systematics found for {systematic_type}")
        return

    systematics_list = " ".join(systematics)
    submit_file_content = condor_submit_content.format(input_directory=input_directory, tag=tag, systematic_type=systematic_type, flavour=flavour, systematics=systematics_list)
    submit_file_name = f"submit/submit_all_systematics_{systematic_type}.sub"
    with open(submit_file_name, "w") as f:
        f.write(submit_file_content)
    
    # Submit the single file
    subprocess.run(["condor_submit", submit_file_name])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Submit HTCondor jobs for processing HDF5 files.")
    parser.add_argument("tag", choices=["mc20d", "mc23a", "mc23c"], help="Specify the r-tag to process (mc20d, mc23a, mc23c).")
    parser.add_argument("systematic_type", choices=["tracking", "jet", "partonshower", "altgen", "all"], help="Specify which type of systematic you're processing (tracking, jet, partonshower, altgen, or all).")
    parser.add_argument("flavour", choices=["b", "c", "l"], help="Specify the flavour to process.")
    args = parser.parse_args()


    tag_map = {
        "mc20d": "r13144",
        "mc23a": "r14622",
        "mc23c": "r14799"
    }

    systematic_types = ['tracking', 'partonshower', 'altgen'] if args.systematic_type == 'all' else [args.systematic_type]
    for systematic_type in systematic_types:
        input_directory = f"../../../data/{systematic_type}"

        main(input_directory, args.tag, tag_map, systematic_type, args.flavour)