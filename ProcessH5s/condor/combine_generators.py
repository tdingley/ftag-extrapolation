#!/usr/bin/env python3

import os
import ROOT as root
import logging
import argparse

# Setup logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def combine_histograms(root_file_path: str, output_file_path: str):
    """Combine histograms from nominal and varied folders in a ROOT file."""
    root_file = root.TFile.Open(root_file_path, "READ")
    output_file = root.TFile.Open(output_file_path, "RECREATE")

    def combine_histograms_from_dirs(directories, output_dir_name):
        combined_histograms = {}
        for directory in directories:
            dir = root_file.Get(directory)
            for key in dir.GetListOfKeys():
                obj = key.ReadObj()
                if isinstance(obj, root.TH1):
                    hist_name = obj.GetName()
                    if hist_name not in combined_histograms:
                        combined_histograms[hist_name] = obj.Clone()
                        combined_histograms[hist_name].SetDirectory(0)  # Detach from the current directory
                    else:
                        combined_histograms[hist_name].Add(obj)

        output_file.mkdir(output_dir_name)
        output_file.cd(output_dir_name)
        for hist in combined_histograms.values():
            hist.Write()

    # Get all directory names
    dir_names = [key.GetName() for key in root_file.GetListOfKeys()]

    # Separate nominal and varied directories
    nominal_dirs = [name for name in dir_names if name.startswith("50")]
    varied_dirs = [name for name in dir_names if name.startswith("70")]

    # Combine histograms
    combine_histograms_from_dirs(nominal_dirs, "nominal")
    combine_histograms_from_dirs(varied_dirs, "Sherpa")

    # Close the ROOT files
    output_file.Close()
    root_file.Close()

    logging.info(f"Histograms combined into '{output_file_path}' in folders 'nominal' and 'Sherpa'.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Combine histograms from nominal and varied folders in a ROOT file.")
    parser.add_argument("root_file", help="Path to the input ROOT file.")
    parser.add_argument("output_file", help="Path to the output ROOT file.")
    args = parser.parse_args()

    combine_histograms(args.root_file, args.output_file)