#!/bin/bash

# Setup environment
export ATLAS_LOCAL_ROOT_BASE='/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase'
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "views LCG_104a x86_64-el9-gcc12-opt"

# Run the Python script with the provided arguments
./process_systematic.py "$@"