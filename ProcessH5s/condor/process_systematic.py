#!/usr/bin/env python3

import os
import h5py
import pandas as pd
import numpy as np
import ROOT as root
import time
import logging
from typing import List, Tuple
import argparse

# Setup logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def calculate_dphi(phi1: float, phi2: float) -> float:
    dphi = np.abs(phi1 - phi2)
    return np.minimum(dphi, 2 * np.pi - dphi)

def get_cutvalue(tagger: str = "GN2v01", 
                 cdi_file: str = '/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2023-02_MC20_CDI_GN2v01-noSF.root', 
                 jet_collection: str = "AntiKt4EMPFlowJets", 
                 wp: int = 65) -> Tuple[List[float], float, float]:
    """Retrieve cut values and fractions from the CDI file."""
    start_time = time.time()

    cdi_file = root.TFile.Open(cdi_file)
    if not cdi_file or cdi_file.IsZombie():
        raise RuntimeError("Failed to open CDI file.")
    
    cut_value_arr = []
    TVectorT_float = root.TVectorT('float')

    for wp in [65, 70, 77, 85, 90]:
        dir_path = f"/{tagger}/{jet_collection}/FixedCutBEff_{wp}/"
        dir = cdi_file.GetDirectory(dir_path)
        if not dir:
            raise RuntimeError(f"Failed to navigate to directory: {dir_path}")
        
        logging.info(f"Getting cut value from: {dir_path}")
        
        cut_value = dir.Get("cutvalue")
        if isinstance(cut_value, (root.TVectorD, TVectorT_float)):
            cut_value_arr.append(cut_value[0])
        else:
            raise ValueError(f"Unknown type: {type(cut_value)}")
       
    fc = dir.Get("fraction")
    ftau = dir.Get("fraction_tau")

    if isinstance(fc, (root.TVectorD, TVectorT_float)):
        fc_val = fc[0] 
    else:
        raise ValueError(f"Unknown type: {type(fc)}")
    if isinstance(ftau, (root.TVectorD, TVectorT_float)):
        ftau_val = ftau[0]
    else:
        raise ValueError(f"Unknown type: {type(ftau)}")

    elapsed_time = time.time() - start_time
    logging.info(f"Time taken to scrape data: {elapsed_time} seconds")
    return cut_value_arr, fc_val, ftau_val
def get_pdg_truth_value(flavour):
    if flavour == 'b':
        pdg = 5
    elif flavour == 'c':
        pdg = 4
    elif flavour == 'l':
        pdg = 0
    else:
        raise ValueError("Please specify flavour from b,c,l")
    return pdg

def process_h5_file(file_path: str, systematic: str, bin_edges: List[int], output_folder: str, flavour: str):
    """Process a single HDF5 file."""
    logging.info(f"Processing file: {file_path}")
    #jets_path = f'{systematic}_STANDARD_poor/jets' if systematic else 'STANDARD_poor/jets'
    jets_path = 'jets'

    with h5py.File(file_path, 'r') as h5file:
        if jets_path not in h5file:
            logging.error(f"Error: {jets_path} not found in {file_path}")
            return None

        jets = h5file[jets_path]

        data = {
            'eventNumber': jets['eventNumber'][:],
            'eta': jets['eta'][:],
            'phi': jets['phi'][:],
            'GN2v01_pb': jets['GN2v01_pb'][:],
            'GN2v01_pc': jets['GN2v01_pc'][:],
            'GN2v01_pu': jets['GN2v01_pu'][:],
            'GN2v01_ptau': jets['GN2v01_ptau'][:],
            'pt': jets['pt'][:] / 1e3,
            'HadronConeExclTruthLabelID': jets['HadronConeExclTruthLabelID'][:]
        }
        df = pd.DataFrame(data)
        pdg = get_pdg_truth_value(flavour)

        f_jets = df[df['HadronConeExclTruthLabelID'] == pdg]
        #f_jets = calculate_dphi_min(b_jets, df)
        cut_values, fc, ftau = get_cutvalue()

        discriminant_values = calculate_discriminant(df, fc, ftau)
        df['discriminant'] = discriminant_values
        f_jets['discriminant'] = df.loc[f_jets.index, 'discriminant']

        create_histograms(f_jets, cut_values, bin_edges, systematic, output_folder)

def calculate_discriminant(df: pd.DataFrame, fc: float, ftau: float) -> pd.Series:
    """Calculate the discriminant for each jet."""
    return np.log(df['GN2v01_pb'] / (df['GN2v01_pu'] * (1 - fc - ftau) + df['GN2v01_ptau'] * ftau + df['GN2v01_pc'] * fc))

def create_histograms(b_jets: pd.DataFrame, cut_values: List[float], bin_edges: List[int], systematic: str, output_folder: str):
    """Create and save histograms."""
    def create_histogram(data: pd.Series, title: str, bins: List[int]) -> root.TH1F:
        hist = root.TH1F(title, title, len(bins) - 1, np.array(bins, dtype='d'))
        for value in data:
            hist.Fill(value)
        return hist

    output_file_name = os.path.join(output_folder, f"{systematic}_output.root") if systematic else os.path.join(output_folder, "nominal_output.root")
    os.makedirs(output_folder, exist_ok=True)
    with root.TFile(output_file_name, "RECREATE") as root_file:
        root_file.cd()

        hist_pt_no_cut = create_histogram(b_jets['pt'], "truth", bin_edges)
        hist_pt_no_cut.Write()

        for cut_value, wp in zip(cut_values, [65, 70, 77, 85, 90]):
            tagged_jets = b_jets[b_jets['discriminant'] > cut_value]
            hist_pt_wp = create_histogram(tagged_jets['pt'], f"tagged_{wp}", bin_edges)
            hist_pt_wp.Write()

def combine_root_files(systematics: List[str], output_folder: str):
    """Combine ROOT files into a single file."""
    combined_root_file = root.TFile(os.path.join(output_folder, "tracking_combined.root"), "RECREATE")

    for systematic in systematics:
        output_file_name = os.path.join(output_folder, f"{systematic}_output.root") if systematic else os.path.join(output_folder, "nominal_output.root")
        with root.TFile(output_file_name, "READ") as input_root_file:
            folder_name = systematic if systematic else "nominal"
            combined_root_file.mkdir(folder_name)
            combined_root_file.cd(folder_name)
            for key in input_root_file.GetListOfKeys():
                obj = key.ReadObj()
                obj.Write()
            combined_root_file.cd()

    combined_root_file.Close()
    logging.info(f"All ROOT files combined into '{os.path.join(output_folder, 'tracking_combined.root')}'.")

def main(input_directory: str, tag: str, systematic_type: str, systematic: str, flavour: str):
    """Main function to process HDF5 files for a given systematic."""
    systematics = set()
    if systematic_type == "altgen":
        bin_edges = [0, 400, 3000]
    else:
        bin_edges = [0, 400, 800, 1200, 1600, 2000, 2500, 3000]

    output_folder = f"output/{flavour}/{tag}/{systematic_type}"

    file_paths = []
    for subdir, _, files in os.walk(input_directory):
        if systematic in subdir:
            for file in files:
                if file.endswith("output.h5"):
                    file_path = os.path.join(subdir, file)
                    file_paths.append((file_path, systematic, output_folder))
                    systematics.add(systematic)
    for file_path, sys, out_folder in file_paths:
        process_h5_file(file_path, sys, bin_edges, out_folder, flavour)

    #combine_root_files(list(systematics), output_folder)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process HDF5 files for a given systematic.")
    parser.add_argument("input_directory", help="Input directory containing the HDF5 files.")
    parser.add_argument("tag", help="Specify the r-tag to process (e.g., mc20d, mc23a, mc23c).")
    parser.add_argument("systematic_type", choices=["tracking", "jet", "partonshower", "altgen"], help="Specify which type of systematic you're processing (tracking, jet or partonshower)")
    parser.add_argument("systematic", help="Specify the systematic to process.")
    parser.add_argument("flavour", choices=["b", "c", "l"], help="Specify the flavour to process.")
    args = parser.parse_args()
    print(args)

    main(args.input_directory, args.tag, args.systematic_type, args.systematic, args.flavour)
   