echo "Running Test script"
source ../setup.sh

echo "Producing example hdf5 file for processing"
python GenerateTestInput.py

echo "Making CDI-files, both relative and monotonically increasing uncertainties"
python ../CDI/CDIMaker.py test_input all

echo "Plotting CDI-file inputs"
python ../CDI/CDIPlotter.py test_input

echo "Check the outputs, ensure you're happy with the repository. Thanks :)"