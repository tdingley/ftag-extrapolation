import os
import ROOT
import numpy as np

def create_histogram(name, bins, bin_edges, values):
    hist = ROOT.TH1F(name, name, bins, np.array(bin_edges, dtype=np.float64))
    for i in range(1, bins + 1):
        hist.SetBinContent(i, values[i-1])
    return hist

def create_root_file(file_name, systematics, variations, bins, bin_edges):
    root_file = ROOT.TFile(file_name, "RECREATE")
    
    for var in variations:
        var_folder = root_file.mkdir(var)
        var_folder.cd()
        
        truth_values = np.random.rand(bins) * 100
        var_folder.WriteObject(create_histogram("truth", bins, bin_edges, truth_values), "truth")
        
        for wp in [65, 70, 77, 85, 90]:
            tagged_values = truth_values * (0.5 + 0.1 * (wp % 5)) * (1 + 0.1 * np.random.rand(bins))
            var_folder.WriteObject(create_histogram(f"tagged_{wp}", bins, bin_edges, tagged_values), f"tagged_{wp}")

    root_file.Close()

def main(output_directory):
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    systematics = {
        "tracking": ["nominal", "TRK_variation1", "TRK_variation2"],
        "jet": ["nominal", "JET_variation1", "JET_variation2"],
        "modelling": ["nominal", "MOD_variation1", "MOD_variation2"]
    }
    
    bins = 7
    bin_edges = [0, 400, 800, 1200, 1600, 2000, 2500, 3000]
    
    for sys, variations in systematics.items():
        file_name = os.path.join(output_directory, f"{sys}_combined.root")
        create_root_file(file_name, systematics, variations, bins, bin_edges)

if __name__ == "__main__":
    output_directory = "test_input"
    main(output_directory)