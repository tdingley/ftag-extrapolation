# FTAG-Extrapolation

Repository for producing CDI inputs from extrapolation ntuples, downloaded via Rucio from TDD grid jobs.

## Table of Contents
- [Description](#description)
- [Installation](#installation)
- [Usage](#usage)
- [Features](#features)
- [Contact](#contact)

## Description
FTAG-Extrapolation is a tool designed to produce CDI inputs from extrapolation ntuples. The ntuples are downloaded via Rucio from TDD grid jobs.

## Installation
To get started with FTAG-Extrapolation, follow these steps:

1. Clone the repository:
    ```sh
    git clone https://gitlab.cern.ch/tdingley/ftag-extrapolation.git
    cd ftag-extrapolation
    ```
2. Test repository
   1. Navigate to the `test` directory:
       ```sh
       cd test
       ```
   2. Run the test script:
       ```sh
       source runtest.sh
       ```
3. Making root files
    1. Navigate to ProcessH5s:
    ```sh
    cd ../ProcessH5s
    ```
    2. Run .root file production:
    ```sh
    python processH5.py <directory containing TDD h5 folders>
    python event_processing.py <directory containing TDD h5 folders> <output folder>
    ```
    3. Run CDIMaker and CDIPlotter
    ```sh
    python CDIMaker.py <folder containing .root files from processH5.py>
    python CDIPlotter.py <folder containing output of CDIMaker.py>
    ```


## Features
- Downloads ntuples via Rucio from TDD grid jobs
- Produces CDI inputs from extrapolation ntuples
- Plots extrapolation and relative uncertainties 


## Contact
For any questions or suggestions, please contact T. Dingley at [thomas.dingley@cern.ch](mailto:thomas.dingley@cern.ch).