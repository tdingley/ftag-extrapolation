#!/bin/bash

set -e  # Exit immediately if a command exits with a non-zero status

# Step 1: Setup the repository
echo "Setting up the repository..."
source setup.sh

# Step 2: Submit the jobs
echo "Submitting jobs..."
cd ProcessH5s/condor
python condor_jobs.py mc20d all b
# Uncomment the lines below to submit jobs for other flavors
# python condor_jobs.py mc20d all l
# python condor_jobs.py mc20d all c

# Step 3: Monitor the jobs
echo "Waiting for HTCondor jobs to complete..."

# Function to check the status of HTCondor jobs
function check_jobs() {
    condor_q -submitter dingleyt | grep -E "Total for query:" | awk '{print $4}'
}

# Loop until there are no jobs left in the queue
while true; do
    num_jobs=$(check_jobs)
    if [ "$num_jobs" -eq "0" ]; then
        echo "All jobs are completed."
        break
    else
        echo "$num_jobs jobs remaining. Checking again in 60 seconds..."
        sleep 60
    fi
done

# Step 4: Post-process the outputs
echo "Starting post-processing..."

# Define systematics
systematics=("partonshower" "tracking" "altgen")

# Combine root files for each systematic
for sys in "${systematics[@]}"; do
    if [ "$sys" == "partonshower" ]; then
        python combine_rootfiles.py output/b/mc20d/"$sys" "$sys"
    else
        python combine_rootfiles.py output/b/mc20d/"$sys"
    fi
done

# Ensure the directory exists before removing and recreating
if [ -d "output/b/mc20d/all" ]; then
    rm -rf output/b/mc20d/all
fi
mkdir -p output/b/mc20d/all

# Copy combined root files to the 'all' directory
for sys in "${systematics[@]}"; do
    scp output/b/mc20d/"$sys"/"${sys}_combined.root" output/b/mc20d/all
done

# Move to the CDI directory
cd ../../CDI

# Run CDIMaker and CDIPlotter scripts
python CDIMaker.py ../ProcessH5s/condor/output/b/mc20d/all all
python CDIPlotter.py ../ProcessH5s/condor/output/b/mc20d/all

echo "Post-processing completed."
