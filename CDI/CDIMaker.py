import os
import ROOT
import json
import numpy as np
import argparse

def calculate_efficiency(tagged_hist, truth_hist):
    """
    Calculate the efficiency by dividing the tagged histogram by the truth histogram.
    
    Parameters:
    - tagged_hist (ROOT.TH1F): Tagged histogram.
    - truth_hist (ROOT.TH1F): Truth histogram.
    
    Returns:
    - ROOT.TH1F: Efficiency histogram.
    """
    efficiency = tagged_hist.Clone()
    efficiency.Divide(truth_hist)
    return efficiency

def process_histograms(nominal_folder, variation_folder, wp):
    """
    Process nominal and variation histograms to compute efficiencies and their differences.
    
    Parameters:
    - nominal_folder (ROOT.TDirectory): Directory containing nominal histograms.
    - variation_folder (ROOT.TDirectory): Directory containing variation histograms.
    - wp (int): Working point percentage.
    
    Returns:
    - dict: Results of efficiency differences.
    - dict: Monotonic results of efficiency differences.
    """
    results = {}
    monotonic_results = {}
    
    truth_hist_nominal = nominal_folder.Get("truth")
    tagged_hist_nominal = nominal_folder.Get(f"tagged_{wp}")
    eff_nominal = calculate_efficiency(tagged_hist_nominal, truth_hist_nominal)
    
    truth_hist_variation = variation_folder.Get("truth")
    tagged_hist_variation = variation_folder.Get(f"tagged_{wp}")
    eff_variation = calculate_efficiency(tagged_hist_variation, truth_hist_variation)
    
    running_max = []
    reference_diff = None
    bins = [eff_nominal.GetBinLowEdge(i) for i in range(1, eff_nominal.GetNbinsX() + 2)]
    
    for bin_idx in range(1, eff_nominal.GetNbinsX() + 1):
        eff_nominal_val = eff_nominal.GetBinContent(bin_idx)
        eff_variation_val = eff_variation.GetBinContent(bin_idx)
        eff_diff = (eff_variation_val - eff_nominal_val) / eff_nominal_val
        
        if bins[bin_idx] == 400:
            reference_diff = eff_diff
        
        delta_eff = eff_diff - (reference_diff if reference_diff is not None else 0)

        if running_max:
            running_max.append(max(running_max[-1], abs(delta_eff)))
        else:
            running_max.append(abs(delta_eff))
        
        bin_label = f"bin_{int(bins[bin_idx-1])}_{int(bins[bin_idx])}"
        
        if bin_label not in results:
            results[bin_label] = {}
        results[bin_label][variation_folder.GetName()] = delta_eff
        
        if bin_label not in monotonic_results:
            monotonic_results[bin_label] = {}
        monotonic_results[bin_label][variation_folder.GetName()] = running_max[-1]
    
    return results, monotonic_results

def save_results(output_directory, wp, results, monotonic_results):
    """
    Save results to JSON files.
    
    Parameters:
    - output_directory (str): Directory to save the output files.
    - wp (int): Working point percentage.
    - results (dict): Results to save.
    - monotonic_results (dict): Monotonic results to save.
    """
    wp_directory = os.path.join(output_directory, f"CDI_{wp}")
    os.makedirs(wp_directory, exist_ok=True)

    output_file_path = os.path.join(wp_directory, f"CDI_{wp}.txt")
    with open(output_file_path, 'w') as output_file:
        json.dump(results, output_file, indent=4)

    monotonic_output_file_path = os.path.join(wp_directory, f"CDI_{wp}_monotonic.txt")
    with open(monotonic_output_file_path, 'w') as monotonic_output_file:
        json.dump(monotonic_results, monotonic_output_file, indent=4)

def process_systematic(input_directory, systematic_type, wps):
    results = {wp: {} for wp in wps}
    monotonic_results = {wp: {} for wp in wps}

    
    # Define maps for each systematic type
    varied_maps = {
        "tracking": ["TRK_"],
        "jet": ["JET_"],
        "partonshower": ["525819"],
        "altgen": ["nominal"]
    }

    nominal_maps = {
        "tracking": ["nominal"],
        "jet": ["nominal"],
        "partonshower": ["521296"],
        "altgen": ["nominal"]
    }
    
    for systematic_type in systematic_type:
        root_file_path = os.path.join(input_directory, f"{systematic_type}_combined.root")
        root_file = ROOT.TFile.Open(root_file_path)
        nominal_folder = root_file.Get(nominal_maps[systematic_type][0])
        variations = [key.GetName() for key in root_file.GetListOfKeys() if key.GetName() not in nominal_maps[systematic_type][0]]

        for variation in variations:
            variation_folder = root_file.Get(variation)
            for wp in wps:
                res, mono_res = process_histograms(nominal_folder, variation_folder, wp)
                for bin_label in res:
                    if bin_label not in results[wp]:
                        results[wp][bin_label] = {}
                    results[wp][bin_label].update(res[bin_label])
                    
                    if bin_label not in monotonic_results[wp]:
                        monotonic_results[wp][bin_label] = {}
                    monotonic_results[wp][bin_label].update(mono_res[bin_label])

        systematic_output_dir = os.path.join(input_directory, systematic_type)
        os.makedirs(systematic_output_dir, exist_ok=True)

        for wp in wps:
            save_results(systematic_output_dir, wp, results[wp], monotonic_results[wp])

def main(input_directory, systematic):
    """
    Main function to process ROOT files and calculate efficiencies for different systematic variations and working points.
    
    Parameters:
    - input_directory (str): Directory containing the input ROOT files.
    - systematic (str): Systematic variation to process ('tracking', 'jet', 'modelling', 'all').
    """
    systematics = ['tracking', 'partonshower'] if systematic == 'all' else [systematic]
    wps = [65, 70, 77, 85, 90]
    process_systematic(input_directory, systematics, wps)
    #for syst in systematics:
    #    process_systematic(input_directory, syst, wps)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process ROOT files to calculate efficiencies for different systematic variations and working points.")
    parser.add_argument("input_directory", help="Directory containing the input ROOT files.")
    parser.add_argument("systematic_type", choices=['tracking', 'partonshower', 'altgen', 'all'], help="Systematic variation to process ('tracking', 'jet', 'partonshower','altgen', 'all').")

    args = parser.parse_args()
        
    main(args.input_directory, args.systematic_type)