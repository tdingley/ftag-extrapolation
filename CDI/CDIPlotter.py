import os
import json
import matplotlib.pyplot as plt
import numpy as np
import atlas_mpl_style as ampl
import argparse

ampl.use_atlas_style()
import matplotlib.font_manager as fm

plt.rcParams['font.family'] = 'DejaVu Sans'

def plot_variations(input_directory):
    """
    Plot the variations of efficiencies for different working points (WPs) and systematic uncertainties.
    
    Parameters:
    - input_directory (str): Directory containing the input data files.
    """
    wps = [65, 70, 77, 85, 90]
    systematics = {'tracking': 'TRK_', 'partonshower': '525819', 'altgen': 'Sherpa'}

    def read_data(file_path):
        """
        Read JSON data from a file.

        Parameters:
        - file_path (str): Path to the JSON file.

        Returns:
        - dict: Parsed JSON data.
        """
        with open(file_path, 'r') as f:
            return json.load(f)

    def aggregate_systematic_data(data, sys_prefix):
        """
        Aggregate systematic uncertainties for a given prefix by summing in quadrature.

        Parameters:
        - data (dict): Data containing variations.
        - sys_prefix (str): Systematic prefix to filter and aggregate.

        Returns:
        - dict: Aggregated data.
        """
        aggregated_data = {bin_label: 0 for bin_label in data.keys()}
        for bin_label, variations in data.items():
            for variation, value in variations.items():
                if variation.startswith(sys_prefix):
                    aggregated_data[bin_label] += value**2
        return {bin_label: np.sqrt(val) for bin_label, val in aggregated_data.items()}

    def compute_quadrature_sum(data):
        """
        Compute the quadrature sum of all variations for each bin.

        Parameters:
        - data (dict): Data containing variations.

        Returns:
        - dict: Quadrature sum of variations for each bin.
        """
        quadrature_sum = {bin_label: 0 for bin_label in data.keys()}
        for bin_label, variations in data.items():
            for value in variations.values():
                quadrature_sum[bin_label] += value**2
        return {bin_label: np.sqrt(val) for bin_label, val in quadrature_sum.items()}

    def find_max_y_value(data):
        """
        Find the maximum y-value in the data.

        Parameters:
        - data (dict): Data containing variations.

        Returns:
        - float: Maximum y-value.
        """
        return max((max(variations.values()) for variations in data.values() if variations.values()), default=0)

    def plot_data(wp, output_directory, systematics, bin_edges, data, quadrature_sum_data, ylim, suffix):
        """
        Plot the data for a given working point (WP).

        Parameters:
        - wp (int): Working point percentage.
        - output_directory (str): Directory to save the plot.
        - systematics (dict): Dictionary of systematic prefixes and names.
        - bin_edges (list): List of bin edges.
        - data (dict): Data to plot.
        - quadrature_sum_data (dict): Quadrature sum data to plot.
        - ylim (float): Y-axis limit for the plot.
        - suffix (str): Suffix for the output file name.
        """
        fig, ax = plt.subplots(figsize=(14, 7))
        bin_labels = list(data.keys())

        for sys_name, sys_prefix in systematics.items():
            print(sys_prefix)
            sys_data = aggregate_systematic_data(data, sys_prefix)
            print(sys_data)
            y_values = [sys_data[bin_label] for bin_label in bin_labels] + [0]
            print(y_values)
            print([sys_data[bin_label] for bin_label in bin_labels])
            ax.step(bin_edges, y_values, where='post', label=sys_name)

        y_values = [quadrature_sum_data[bin_label] for bin_label in bin_labels] + [0]
        ax.step(bin_edges, y_values, where='post', label='Total', linewidth=2, linestyle='-', color='black')

        ax.set_xlabel(r'$p_T$ [GeV]')
        ax.set_ylabel('Extrapolation Uncertainty')
        ax.set_xlim(400, 3000)
        if suffix == 'monotonic':
            ax.set_ylim(0, ylim)
        else:
            ax.set_ylim(-ylim, ylim)
        ax.axhline(0, linestyle='--', color='black')
        ax.legend(loc='upper left', bbox_to_anchor=(1, 1), fontsize=12)

        ampl.draw_atlas_label(
            0.05, 0.95, ax=ax, status='int', simulation=True, 
            energy='13 TeV', lumi=140, 
            desc=f"AntiKt4EMPFlowJets, Fixed Cut {wp}% WP \nExtrapolation Uncertainties" if suffix == 'monotonic' else f"AntiKt4EMPFlowJets, Fixed Cut {wp}% WP \nRelative Uncertainties", fontsize=15
        )

        plt.tight_layout(rect=[0, 0, 0.85, 1])
        plt.savefig(os.path.join(output_directory, f'CDI_{wp}_all_bins_{suffix}.pdf'))
        plt.close()

    def plot_component_data(wp, output_directory, data, suffix):
        """
        Plot the component data for a given working point (WP).

        Parameters:
        - wp (int): Working point percentage.
        - output_directory (str): Directory to save the plot.
        - data (dict): Data to plot.
        - suffix (str): Suffix for the output file name.
        """
        fig, ax = plt.subplots(figsize=(14, 7))
        bin_labels = list(data.keys())
        bin_edges = [int(b.split('_')[1]) for b in bin_labels]
        bin_edges.append(int(bin_labels[-1].split('_')[2]))

        for variation in data[next(iter(data))].keys():
            if variation == '525819': 
                print(suffix)
                variation_name = "Herwig7"
            else:
                variation_name = variation

            y_values = [data[bin_label][variation] for bin_label in bin_labels] + [0]
            ax.step(bin_edges, y_values, where='post', label=variation_name)

        ylim = 1.5 * max((max(data[bin_label][variation] for variation in data[bin_label]) for bin_label in data if data[bin_label]), default=0)

        ax.set_xlabel(r'$p_T$ [GeV]')
        ax.set_ylabel('Delta Efficiency Difference')
        ax.set_xlim(400, 3000)
        ax.set_ylim(-ylim, ylim)
        ax.axhline(0, linestyle='--', color='black')
        ax.legend(loc='upper left', bbox_to_anchor=(1, 1), fontsize=12)

        ampl.draw_atlas_label(
            0.05, 0.95, ax=ax, status='int', simulation=True, 
            energy='13 TeV', lumi=140, 
            desc=f"AntiKt4EMPFlowJets, Fixed Cut {wp}% WP \nRelative Uncertainties", fontsize=15
        )

        plt.tight_layout(rect=[0, 0, 0.85, 1])
        plt.savefig(os.path.join(output_directory, f'CDI_{wp}_components_{suffix}.pdf'))
        plt.close()
    
    for systematic, sys_name in systematics.items():
        for wp in wps:
            systematic_dir = os.path.join(input_directory, systematic, f'CDI_{wp}')
            try:
                monotonic_data = read_data(os.path.join(systematic_dir, f"CDI_{wp}_monotonic.txt"))
                normal_data = read_data(os.path.join(systematic_dir, f"CDI_{wp}.txt"))
            except Exception as e:
                print(f"Error reading data for WP {wp} in {systematic}: {e}")
                continue

            quadrature_sum_data = compute_quadrature_sum(monotonic_data)
            max_y_value = max(find_max_y_value(monotonic_data), max(quadrature_sum_data.values(), default=0))
            ylim = 1.5 * max_y_value
            bin_labels = list(monotonic_data.keys())
            bin_edges = [int(b.split('_')[1]) for b in bin_labels]
            bin_edges.append(int(bin_labels[-1].split('_')[2]))

            plot_data(wp, systematic_dir, systematics, bin_edges, monotonic_data, quadrature_sum_data, ylim, 'monotonic')
            plot_component_data(wp, systematic_dir, normal_data, 'normal')

            # Plot each systematic variation separately
            try:
                data = read_data(os.path.join(systematic_dir, f"CDI_{wp}.txt"))
            except Exception as e:
                print(f"Error reading data for WP {wp} in {systematic}: {e}")
                continue
            """
            fig, ax = plt.subplots(figsize=(14, 7))
            bin_labels = list(data.keys())
            bin_edges = [int(b.split('_')[1]) for b in bin_labels]
            bin_edges.append(int(bin_labels[-1].split('_')[2]))

            for variation in data[next(iter(data))].keys():
                if variation.startswith(systematic):
                    y_values = [data[bin_label][variation] for bin_label in bin_labels] + [0]
                    ax.step(bin_edges, y_values, where='post', label=variation)

            ylim = 1.5 * max((max(data[bin_label][variation], default=0) for bin_label in data if data[bin_label] and variation.startswith(systematic)), default=0)

            ax.set_xlabel(r'$p_T$ [GeV]')
            ax.set_ylabel('Delta Efficiency Difference')
            ax.set_xlim(400, 3000)
            #ax.set_ylim(0, ylim)
            ax.axhline(0, linestyle='--', color='black')
            ax.legend(loc='upper left', bbox_to_anchor=(1, 1), fontsize=12)

            ampl.draw_atlas_label(
                0.05, 0.95, ax=ax, status='int', simulation=True, 
                energy='13 TeV', lumi=140, 
                desc=f"AntiKt4EMPFlowJets, Fixed Cut {wp}% WP \nRelative Uncertainties", fontsize=15
            )

            plt.tight_layout(rect=[0, 0, 0.85, 1])
            print(sys_name)
            print(f"Making figure for {sys_name}.")
            #plt.savefig(os.path.join(systematic_dir, f'CDI_{wp}_{sys_name}.pdf'))
            plt.close()
            """

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot variations of efficiencies for different working points and systematic uncertainties.")
    parser.add_argument("input_directory", help="Directory containing the input data files.")

    args = parser.parse_args()
    plot_variations(args.input_directory)